# Homework 6

### Due Wed 3/3/21 @ 12a


In this homework, we will finish re-tracing Megi's steps: using his source code to
re-build a boot image from scratch by ourselves. But we will do a few things differently.
See Homework 5 for more details about setting up the build environment.

1.	We will only provide the Lune-OS operating system inside the P-Boot menu
2.	We will use the latest release of Lune-OS
3.	We will use the latest Linux Kernel


##### Setup build scripts:

In the previous homework, you downloaded the build scripts for the 
Multi-boot-image. **Make sure that your working directory is NOT inside a VirtualBox shared directory.** If it is, you'll have to re-clone the source-code repos into a better place. (The home directory `~` is a good place: `~/LuneImgBuild`)  

cd into the multiboot dir `cd LuneImgBuild/pinephone-multi-boot`
take a look at the README file.

note: take a look at the directory structure Megi uses.  His build scripts will expect
this structure.  In the previous homework, we named the directory holding our kernel
"kernels."  Megi names his "builds".  Let's rename ours
- `mv LuneImgBuild/kernels LuneImgBuild/builds`

note: Megi provides a command for renaming instances of text in a file. We will have to use
this later to replace all instances of `ppd-5.10` with `pp-5.11` because we are using a more
recent kernel... keep this in mind.

- we won't be using any distros other than Lune. So, cd into the `distros` directory and remove all directories for distros **Except Lune**  also **DO NOT REMOVE  the `extractX.sh`** files

> [5pts] After removal, screencap the output of `ls -al` inside `distros` (distros.png)

We now have to modify some scripts to use a more up-to-date version of LUNE OS.
First go to http://build.webos-ports.org/luneos-testing/images/pinephone/ to see all the pre-built Lune images (this is what we did in HW5).  The maintainers have scripts which regularly 
rebuild images.

Pick a release. I'm going to pick the most recent, ver.140 from Feb 13 2021.  Note the version number and release date from the URL to the `*.wic.gz` file. For example: http://build.webos-ports.org/luneos-testing/images/pinephone/luneos-dev-image-pinephone-testing-0-140.rootfs.wic.gz.md5 has version number 140 and was released on Feb 13,2021.

- go into the `distros/lune` directory and edit `config`.  Make sure the following lines are *at the top of the file*:

```bash
version=YYYY-MM-DD    # make this the date of the version you want to install
VERSION=NNN			 # make this the version number of the version you want
```
> [5pts] screencap the file (config.png)

- in the same directory, edit `README` and add the following lines *at the top*:

```bash
parent_dir="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source "${parent_dir}/config"

```

##### Extract Kernel:

CD into `LuneImgBuild/builds` and make sure to extract the kernel you downloaded in HW5:
```bash
tar -xzf pp.tar.gz
```

##### Try to Run the build script:
CD back to `pinephone-multi-boot` and try to run `bash ./mkimage.sh`.  You will get an error that says "line 4: unsupported command"

To decipher this problem, look higher up in the output of the last command at the lines that start with "+" these lines correspond to lines in the `mkimage.sh` script.
Looking at `mkimage.sh`, we see the following:

```bash
sfdisk -W always $IMG <<EOF
label: dos
label-id: 0x12345678
unit: sectors
sector-size: 512

4M,124M,L,*
128M,,L
EOF
```

This is called a HEREDOC.  The characters `<<EOF` signal that all of the following text (including newlines) up to the characters `EOF` are to be piped into the stdin of the preceding command. 

Thus, everything from `label: ...` to `EOF` is given as input to the `sfdisk` command.
So, when `sfdisk` complains of a bad command on "line 4", it means line 4 of the heredoc, 
which is `sector-size: 512`.  Let's remove that line and try again.

Try `bash mkimage.sh` again.  You will probably get the following error this time:

```bash
+ mkfs.btrfs /dev/loop0p2
mkimage.sh: line 25: mkfs.btrfs: command not found
```

`command not found` errors usually mean that a script tried to run a program that you don't have.
You can install `mkfs.btrfs` with the command `sudo apt install btrfs-progs`.

Try `bash mkimage.sh` again. You will probably get the following error this time:
```bash
+ bsdtar -xp --numeric-owner -C m/lune -f distros/lune/rootfs.tar.zst
./mkimage.sh: line 35: bsdtar: command not found
```
here we have another "command not found". We need to install something called `bsdtar`.
A little googling reveals that in Ubuntu, the BSD version of the `tar` utility is
distributed in a package called 'libarchive-tools'.  Let's install that.

`sudo apt install libarchive-tools`

Try `bash ./mkimage.sh` again. This time, you will probably get the error
```
+ bsdtar -xp --numeric-owner -C m/lune -f distros/lune/rootfs.tar.zst
bsdtar: Error opening archive: Failed to open 'distros/lune/rootfs.tar.zst'
```
And so it appears that the build script is looking for a `*.tar.zst` archive in `distros/lune/` which isn't there.  Let's go see where it should come from...







##### Prepare the Lune OS image

Let's CD back to `distros/lune`.  Looking at the README file, we see that this is a script.
The script uses `curl` to download the image file for us. It then uses `gzip` to decompress
the downloaded file and runs the `../extract2.sh` script on the result.  

- take a look at the `../extract2.sh` script.  (remember that `..` means "parent directory" or "up one level")

- here you see that Megi's `extract2.sh` script also uses the `bsdtar` command as well as `zstd` which redirects its output to a new file.

> [5pts] What is the name of the new file that the `zstd` command creates (via output redirection) ?

-- rootfs.tar.zst

Check to be sure your system can find the `zstd` command: `which zstd`.
If the response is "zstd not found", then you'll have to install it `sudo apt install zstd`

CD back to `distros/lune` and run the README script:
`bash README`

This will download and prepare the prebuilt LuneOS image for us using our `config` variables and
the `extract2.sh` script.

When this step is complete, we should have an image of the full Lune root file system in a new file called `rootfs.tar.zst`

> [10pts] screenshot the output of `ls -alh` showing the new `rootfs.tar.zst` file (rootfs.png)





##### Try again to build the Multiboot image

Go back to the `pinephone-multi-boot` directory and try to run the image building script again:
`bash mkimage.sh`

This time, I got the error:

```
+ ../p-boot/.build/p-boot-conf-native . /dev/loop3p1
./mkimage-boot.sh: line 60: ../p-boot/.build/p-boot-conf-native: No such file or directory
```

In this case, the directory `../p-boot` is where the bootloader source code lives.  It seems like we might have to build the bootloader first. Let's go take a look there.










##### Look into the P-Boot bootloader:

(If you previously cloned the P-boot source code into a subdirectory within a VirtualBox shared folder, then you'll have to re-clone it somewhere else.  **IT WILL NOT WORK IF YOU MOVE IT**)

`cd ../p-boot`

Let's take a look at the `README` there.  According to the readme, we're going to need some tools

```
sudo apt install ninja-build php gcc-aarch64-linux-gnu
```

Now, we have to create the `config.ini` file based on the provided `config.ini.sample`.
First edit the `config.ini.sample` file so that it points to the cross-compiler we just
installed:

```
# config.ini.sample:
aarch64_prefix = aarch64-linux-gnu-
```

then rename the file: `mv config.ini.sample config.ini`

Attempt to build P-Boot bootloader:
```
php configure.php
ninja
```

It will probably fail with the message: 
```
/bin/sh: arm-none-eabi-gcc: command not found
```

Let's install the GCC tools for the ARM EABI:
```
sudo apt install gcc-arm-none-eabi
```

Try again to build P-Boot: `ninja`

If you did everything right, the build will conclude with the message:

```
[187/187] OBJDUMP .build/p-boot/bin.as

```

> [15 pts] screenshot the last few lines of successful build (pbootbuild.png)





##### Try again to build the Multiboot image


Go back to the `pinephone-multi-boot` directory and try to run the image building script again:
`bash mkimage.sh`

This time, the build failed with the message: 
```
ERROR: Can't resolve path './../builds/ppd-5.10/board-1.1.dtb
```

This is familiar... remember Megi's message in the multi-boot README where he said if you use a newer kernel, you'll have to update the scripts??  Well, it looks like a script is trying to find a file from the 'ppd-5.10' kernel build. We're using version 5.11.  Let's use Megi's suggested `sed` command to find and replace all references to "pp-5.10" with "pp-5.11".
Alternatively, you can just manually edit `mkimage-boot.sh` and replace the references by hand.

```
cd pinephone-multi-boot/
sed -i 's/ppd-5.10/pp-5.11/g' mkimage-boot.sh
```

(If you chose a different prebuilt kernel from Megi, you would use something other than "pp-5.11" in the above command)



##### Try again to build the Multiboot image


Go back to the `pinephone-multi-boot` directory and try to run the image building script again:
`bash mkimage.sh`


If all goes well, you'll see the following:

```
...

  0645f000-06853800 pboot2.argb /home/user/LuneImgBuild/pinephone-multi-boot/files/pboot2.argb

Total filesystem size 106830 KiB

+ losetup -d /dev/loop10
+ dd if=../p-boot/.build/p-boot.bin of=/tmp/multi.img bs=1024 seek=8 conv=notrunc
32+0 records in
32+0 records out
32768 bytes (33 kB, 32 KiB) copied, 0.00166012 s, 19.7 MB/s

```

and you will have an image at `/tmp/multi.img`.

> [15 pts] screencap the output of `ls -alh /tmp/multi.img` (multiimg.png)





##### Flash your fresh image to the SD card and boot the phone!!!

You've done this before.  In this case, if you're running the flashing utility on your host OS (eg. Win10), you'll have to first copy your image to your Virtual Box shared folder to get it into the host's file system:

`cp /tmp/multi.img /SHARED`
(in my case, /SHARED is a VB shared folder)


>[15 pts] Take a picture of your phone showing the main P-Boot Menu (bootmenu.jpg)
>[10 pts] Take a picture of your phone showing the luneOS desktop (desktop.jpg)
